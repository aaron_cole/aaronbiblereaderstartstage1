package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.org.apache.bcel.internal.generic.DALOAD;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author Aaron Cole
 */

// Make sure you test at least the following:
// --All of the constructors and all of the methods.
// --The equals for success and for failure, especially when they match in 2 of the 3 places
//    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
// --That compareTo is ordered properly for similar cases as the equals tests.
//    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
// --Any other potentially tricky things.

public class Stage01StudentReferenceTest {

	private Reference ruth1_2;
	private Reference ruth1_3;
	private Reference ruth4_2;
	private Reference rom1_2;
	private Reference gen3_23;
	private Reference rev11_4;
	private Reference cor2_7_10;
	private Reference john1_4_5;

	@Before
	public void setUp() throws Exception {
		ruth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		ruth1_3 = new Reference(BookOfBible.Ruth, 1, 3);
		ruth4_2 = new Reference(BookOfBible.Ruth, 4, 2);
		rom1_2 = new Reference(BookOfBible.Romans, 1, 2);
		gen3_23 = new Reference(BookOfBible.Genesis, 3, 23);
		rev11_4 = new Reference(BookOfBible.Revelation, 11, 4);
		cor2_7_10 = new Reference(BookOfBible.Corinthians2, 7, 10);
		john1_4_5 = new Reference(BookOfBible.John1, 4, 5);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		Reference acts8_17 = new Reference(BookOfBible.Acts, 8, 17);
		assertEquals(BookOfBible.Acts, acts8_17.getBookOfBible());
		assertEquals(8, acts8_17.getChapter());
		assertEquals(17, acts8_17.getVerse());
	}

	@Test
	public void testGetters() {
		assertEquals("Revelation", rev11_4.getBook());
		assertEquals("Genesis", gen3_23.getBook());
		assertEquals("1 John", john1_4_5.getBook());

		assertEquals(BookOfBible.Corinthians2, cor2_7_10.getBookOfBible());
		assertEquals(BookOfBible.Genesis, gen3_23.getBookOfBible());
		assertEquals(BookOfBible.Ruth, ruth1_2.getBookOfBible());

		assertEquals(1, ruth1_2.getChapter());
		assertEquals(11, rev11_4.getChapter());
		assertEquals(4, john1_4_5.getChapter());

		assertEquals(23, gen3_23.getVerse());
		assertEquals(4, rev11_4.getVerse());
		assertEquals(10, cor2_7_10.getVerse());
	}

	@Test
	public void testEqualsAndHashCode() {
		assertTrue(ruth1_2.equals(new Reference(BookOfBible.Ruth, 1, 2)));
		assertFalse(ruth1_2.equals(ruth1_3));
		assertFalse(ruth1_2.equals(ruth4_2));
		assertFalse(ruth1_2.equals(rom1_2));

		assertEquals("Ruth 1:2".hashCode(), ruth1_2.hashCode());
		assertEquals("Ruth 1:3".hashCode(), ruth1_3.hashCode());
		assertEquals("Ruth 4:2".hashCode(), ruth4_2.hashCode());
		assertEquals("Romans 1:2".hashCode(), rom1_2.hashCode());
	}

	@Test
	public void testCompareTo() {
		assertEquals(0, gen3_23.compareTo(new Reference(BookOfBible.Genesis, 3, 23)));
		assertEquals(-34, gen3_23.compareTo(new Reference(BookOfBible.Habakkuk, 3, 23)));
		assertEquals(-47, gen3_23.compareTo(new Reference(BookOfBible.Galatians, 3, 23)));
		assertEquals(1, gen3_23.compareTo(new Reference(BookOfBible.Genesis, 2, 23)));
		assertEquals(-1, gen3_23.compareTo(new Reference(BookOfBible.Genesis, 4, 23)));
		assertEquals(1, gen3_23.compareTo(new Reference(BookOfBible.Genesis, 3, 22)));
		assertEquals(-1, gen3_23.compareTo(new Reference(BookOfBible.Genesis, 3, 24)));
	}

	@Test
	public void testToString() {
		assertEquals("Genesis 3:23", gen3_23.toString());
		assertEquals("Revelation 11:4", rev11_4.toString());
		assertEquals("1 John 4:5", john1_4_5.toString());
	}

}
