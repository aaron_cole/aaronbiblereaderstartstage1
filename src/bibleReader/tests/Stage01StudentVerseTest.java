package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;

/*
 * Tests for the Verse class.
 * @author Aaron Cole
 */

public class Stage01StudentVerseTest {

	private Verse ruth1_2;
	private Verse ruth1_3;
	private Verse ruth4_2;
	private Verse rom1_2;
	private Verse gen3_23;
	private Verse rev11_4;
	private Verse cor2_7_10;
	private Verse john1_4_5;

	/*
	 * Anything that should be done before each test. For instance, you might create
	 * some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// TODO Create objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
		ruth1_2 = new Verse(BookOfBible.Ruth, 1, 2, "Sample text for Ruth 1:2");
		ruth1_3 = new Verse(BookOfBible.Ruth, 1, 3, "Sample text for Ruth 1:3");
		ruth4_2 = new Verse(BookOfBible.Ruth, 4, 2, "Sample text for Ruth 4:2");
		rom1_2 = new Verse(BookOfBible.Romans, 1, 2, "Sample text for Romans 1:2");
		gen3_23 = new Verse(BookOfBible.Genesis, 3, 23, "Sample text for Genesis 3:23");
		rev11_4 = new Verse(BookOfBible.Revelation, 11, 4, "Sample text for Revelation 11:4");
		cor2_7_10 = new Verse(BookOfBible.Corinthians2, 7, 10, "Sample text for 2 Corinthians 1:10");
		john1_4_5 = new Verse(BookOfBible.John1, 4, 5, "Sample text for 1 John 4:5");
	}

	/*
	 * Anything that should be done at the end of each test. Most likely you won't
	 * need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without
	 * implementation) are given below.
	 */

	@Test
	public void testConstructors() {
		Verse acts8_17 = new Verse(BookOfBible.Acts, 8, 17, "Sample text for Acts 8:17");
		assertEquals(new Reference(BookOfBible.Acts, 8, 17), acts8_17.getReference());
		assertEquals("Sample text for Acts 8:17", acts8_17.getText());

		Verse amos8_17 = new Verse(new Reference(BookOfBible.Amos, 7, 18), "Sample text for Amos 7:18");
		assertEquals(new Reference(BookOfBible.Amos, 7, 18), amos8_17.getReference());
		assertEquals("Sample text for Amos 7:18", amos8_17.getText());
	}

	@Test
	public void testGetters() {
		assertEquals(new Reference(BookOfBible.Ruth, 1, 2), ruth1_2.getReference());
		assertEquals(new Reference(BookOfBible.Revelation, 11, 4), rev11_4.getReference());
		assertEquals(new Reference(BookOfBible.John1, 4, 5), john1_4_5.getReference());

		assertEquals("Sample text for Genesis 3:23", gen3_23.getText());
		assertEquals("Sample text for Revelation 11:4", rev11_4.getText());
		assertEquals("Sample text for 2 Corinthians 1:10", cor2_7_10.getText());
	}

	@Test
	public void testToString() {
		assertEquals("Genesis 3:23 Sample text for Genesis 3:23", gen3_23.toString());
		assertEquals("Revelation 11:4 Sample text for Revelation 11:4", rev11_4.toString());
		assertEquals("1 John 4:5 Sample text for 1 John 4:5", john1_4_5.toString());
	}

	@Test
	public void testEqualsAndHashCode() {
		assertTrue(ruth1_2.equals(new Verse(BookOfBible.Ruth, 1, 2, "Sample text for Ruth 1:2")));
		assertFalse(ruth1_2.equals(ruth1_3));
		assertFalse(ruth1_2.equals(ruth4_2));
		assertFalse(ruth1_2.equals(rom1_2));

		assertEquals("Ruth 1:2 Sample text for Ruth 1:2".hashCode(), ruth1_2.hashCode());
		assertEquals("Ruth 1:3 Sample text for Ruth 1:3".hashCode(), ruth1_3.hashCode());
		assertEquals("Ruth 4:2 Sample text for Ruth 4:2".hashCode(), ruth4_2.hashCode());
		assertEquals("Romans 1:2 Sample text for Romans 1:2".hashCode(), rom1_2.hashCode());
	}

	@Test
	public void testCompareTo() {
		assertEquals(0, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 3, 23, "Sample text for Genesis 3:23")));
		assertEquals(-34, gen3_23.compareTo(new Verse(BookOfBible.Habakkuk, 3, 23, "Sample text for Genesis 3:23")));
		assertEquals(-47, gen3_23.compareTo(new Verse(BookOfBible.Galatians, 3, 23, "Sample text for Genesis 3:23")));
		assertEquals(1, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 2, 23, "Sample text for Genesis 3:23")));
		assertEquals(-1, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 4, 23, "Sample text for Genesis 3:23")));
		assertEquals(1, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 3, 22, "Sample text for Genesis 3:23")));
		assertEquals(-1, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 3, 24, "Sample text for Genesis 3:23")));
		assertEquals(5, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 3, 23, "New text for Genesis 3:23")));
		assertEquals(-1, gen3_23.compareTo(new Verse(BookOfBible.Genesis, 3, 23, "Text for Genesis 3:23")));
	}

	@Test
	public void testSameReference() {
		assertTrue(cor2_7_10
				.sameReference(new Verse(BookOfBible.Corinthians2, 7, 10, "Sample text for 2 Corinthians 1:10")));
		assertFalse(cor2_7_10
				.sameReference(new Verse(BookOfBible.Corinthians2, 7, 9, "Sample text for 2 Corinthians 1:10")));
		assertFalse(cor2_7_10
				.sameReference(new Verse(BookOfBible.Corinthians2, 8, 10, "Sample text for 2 Corinthians 1:10")));
		assertFalse(cor2_7_10
				.sameReference(new Verse(BookOfBible.Corinthians1, 7, 10, "Sample text for 2 Corinthians 1:10")));
	}
}
