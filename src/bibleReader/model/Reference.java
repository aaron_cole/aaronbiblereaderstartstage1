package bibleReader.model;

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Aaron Cole (provided the implementation)
 */
public class Reference implements Comparable<Reference> {

	private BookOfBible book;
	private int chapter;
	private int verse;

	/**
	 * Construct a reference given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * Returns the book name for this Reference.
	 * 
	 * @return A string of the book for this Reference.
	 */
	public String getBook() {
		return book.toString();
	}

	/**
	 * Returns the BookOfBible object for this Reference.
	 * 
	 * @return A BookOfBible object for this Reference.
	 */
	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * Returns the chapter integer for this Reference.
	 * 
	 * @return An integer for this Reference.
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * Returns the verse integer for this Reference.
	 * 
	 * @return An integer for this Reference.
	 */
	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		return book.toString() + " " + chapter + ":" + verse;
	}

	/**
	 * Should return true if and only if they have the same book, chapter, and
	 * verse.
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Reference) {
			Reference ref = (Reference) other;
			if (book.equals(ref.getBookOfBible()) && chapter == ref.getChapter() && verse == ref.getVerse()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the integer for the hashCode of the resulting toString
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Reference otherRef) {
		int netCompare = 0;
		netCompare = book.compareTo(otherRef.getBookOfBible());
		netCompare += chapter - otherRef.getChapter();
		netCompare += verse - otherRef.getVerse();
		return netCompare;
	}
}
