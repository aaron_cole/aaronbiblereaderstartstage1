package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Aaron Cole (provided the implementation)
 */
public class Verse implements Comparable<Verse> {

	private Reference r;
	private String verseText;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		this.r = r;
		verseText = t;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		r = new Reference(book, chapter, verse);
		verseText = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return r;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return verseText;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	@Override
	public String toString() {
		return r.getBook() + " " + r.getChapter() + ":" + r.getVerse() + " " + verseText;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Verse) {
			Verse ver = (Verse) other;
			if (r.getBookOfBible().equals(ver.r.getBookOfBible()) && r.getChapter() == ver.r.getChapter()
					&& r.getVerse() == ver.r.getVerse() && verseText.equals(ver.getText())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		int netCompare = 0;
		netCompare = r.getBookOfBible().compareTo(other.r.getBookOfBible());
		netCompare += r.getChapter() - other.r.getChapter();
		netCompare += r.getVerse() - other.r.getVerse();
		if (netCompare == 0) {
			netCompare += verseText.compareTo(other.getText());
			return netCompare;
		} else {
			return netCompare;
		}
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
